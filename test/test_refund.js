'use strict'

var assert   = require('assert')
var initTest = require('./test_init.js')
var request  = initTest.request
var db       = initTest.db

describe.skip('Refund --', function () {
  describe('API', function () {
    describe('GET /refunds', function () {
      it('should get list of all refunds', function (done) {
        request.get('/refunds')
        .set({'Authorization' : 'Bearer ' + initTest.userToken()})
        .expect(200)
        .expect(function (res) {
          var data = res.body.data
          assert.equal(data.length, 3)

          assert.equal(data[0].id,            100              )
          assert.equal(data[0].description,   'Refund_2_to_1_5')

          assert.equal(data[1].id,            110              )
          assert.equal(data[1].description,   'Refund_2_to_1_7')
          return true
        })
        .end(done)
      })
    })
  })
})
