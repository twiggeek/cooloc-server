'use strict'

var assert   = require('assert')
var initTest = require('./test_init.js')
var request  = initTest.request
var db       = initTest.db

describe('Global --', function () {
  describe('Server', function () {
    it('should be started', function (done) {
      request.get('/').expect(200, done)
    })
  })

  describe('Migration', function () {
    it('should create a schema for the test db', function (done) {
      db.query('SELECT count(*) FROM information_schema.tables WHERE table_schema = \'public\'', function(err, res) {
        assert.equal(err, null)
        assert.equal(res[0].count, 12)
        done()
      })
    })
  })
})
