'use strict'

var assert   = require('assert')
var initTest = require('./test_init.js')
var request  = initTest.request
var db       = initTest.db

describe.skip('Purchase --', function () {
  describe('API', function () {
    describe('GET /purchases', function () {
      it('should get list of all purchases', function (done) {
        request.get('/purchases')
        .set({'Authorization' : 'Bearer ' + initTest.userToken()})
        .expect(200)
        .expect(function (res) {
          var data = res.body.data

          assert.equal(data.length, 14)

          assert.equal(data[0].id,            14             )
          assert.equal(data[0].description,   'purchase_1_5' )
          assert.equal(data[0].price,         100000         )
          assert.equal(data[0].date_created,  '10-11-2014'   )
          assert.equal(data[0].firstname,     'first_user_10')
          assert.equal(data[0].lastname,      'last_user_10' )
          assert.equal(data[0].nickname,      'nick_user_10' )
          assert.equal(data[0].birthdate,     '01-01-2000'   )

          assert.equal(data[1].id,            23)
          assert.equal(data[1].description,   'purchase_2_9' )
          assert.equal(data[1].price,         9              )
          assert.equal(data[1].date_created,  '09-11-2014'   )
          assert.equal(data[1].firstname,     'first_user_20')
          assert.equal(data[1].lastname,      'last_user_20' )
          assert.equal(data[1].nickname,      'nick_user_20' )
          assert.equal(data[1].birthdate,     '01-01-2001'   )
          return true
        })
        .end(done)
      })
    })
  })
})
