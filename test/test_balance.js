'use strict'

var assert = require('assert')
var initTest = require('./test_init.js')
var request = initTest.request
var db = initTest.db

describe.skip('Balance --', function () {
  describe('API', function () {
    describe('GET /balances', function () {
      it('should get list of all balances', function (done) {
        request.get('/balances')
        .set({'Authorization' : 'Bearer ' + initTest.userToken()})
        .expect(200)
        .expect(function (res) {
          var data = res.body.data
          assert.equal(data.length, 2)

          assert.equal(data[0].user_id      , 200                   )
          assert.equal(data[0].firstname    , 'first_user_20'       )
          assert.equal(data[0].lastname     , 'last_user_20'        )
          assert.equal(data[0].nickname     , 'nick_user_20'        )
          assert.equal(data[0].sum_price    , 135                   )
          assert.equal(data[0].sum_refunds  , 189                   )
          assert.equal(data[0].last_purchase, '11-09-2014 00:00:00' )
          assert.equal(data[0].last_refund  , '11-20-2014 00:00:00' )
          assert.equal(data[0].balance      , -54                   )
          return true
        })
        .end(done)
      })
    })
  })
})
