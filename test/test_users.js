'use strict'

var assert   = require('assert')
var initTest = require('./test_init.js')
var request  = initTest.request
var db       = initTest.db

describe('Users --', function () {
  describe('Session', function () {
    it('should not ask for authentication', function (done) {
      request.get('/')
      .expect(200, 'Hello world!')
      .end(done)
    })
    it('should ask for authentication', function (done) {
      request.get('/users')
      .expect(400)
      .expect(function (res) {
        var data = res.body
        assert.equal(data.code,               400)
        assert.equal(data.error,              'invalid_request')
        assert.equal(data.error_description,  'The access token was not found')
        return true
      })
      .end(done)
    })
    it('should fail because of bad token', function (done) {
      request.post('/oauth/token')
      .set({
        'Authorization' : 'Basic czZCaGRSa3F0MzpnWDFmQmF0M2JW',
        'Content-Type' : 'application/x-www-form-urlencoded'
      })
      .send('grant_type=password&username=name_user_10&password=123456')
      .expect(400)
      .expect(function (res) {
        var data = res.body
        assert.equal(data.code,               400)
        assert.equal(data.error,              'invalid_client')
        assert.equal(data.error_description,  'Client credentials are invalid')
        return true
      })
      .end(done)
    })
    it('should fail because of bad username', function (done) {
      request.post('/oauth/token')
      .set({
        'Authorization' : 'Basic ' + initTest.clientToken,
        'Content-Type' : 'application/x-www-form-urlencoded'
      })
      .send('grant_type=password&username=name_user_&password=123456')
      .expect(400)
      .expect(function (res) {
        var data = res.body
        assert.equal(data.code,               400)
        assert.equal(data.error,              'invalid_grant')
        assert.equal(data.error_description,  'User credentials are invalid')
        return true
      })
      .end(done)
    })
    it('should success to connect', function (done) {
      request.post('/oauth/token')
      .set({
        'Authorization' : 'Basic ' + initTest.clientToken,
        'Content-Type' : 'application/x-www-form-urlencoded'
      })
      .send('grant_type=password&username=name_user_10&password=7c4a8d09ca3762af61e59520943dc26494f8941b')
      .expect(200)
      .expect(function (res) {
        var data = res.body
        assert.equal(data.token_type,           'bearer')
        assert.equal(data.access_token.length,  40)
        assert.equal(data.expires_in,           3600)
        return true
      })
      .end(done)
    })
    it('should success to connect and access data', function (done) {
      request.post('/oauth/token')
      .set({
        'Authorization' : 'Basic ' + initTest.clientToken,
        'Content-Type' : 'application/x-www-form-urlencoded'
      })
      .send('grant_type=password&username=name_user_20&password=7c4a8d09ca3762af61e59520943dc26494f8941b')
      .expect(200)
      .end(function (err, res) {
        assert.equal(err, null)
        var data = res.body
        assert.equal(data.token_type,           'bearer')
        assert.equal(data.access_token.length,  40)
        assert.equal(data.expires_in,           3600)
        request.get('/user/200')
        .set({
          'Authorization' : 'Bearer ' + data.access_token
        })
        .expect(200)
        .end(done)
      })
    })
  })

  describe('Schema', function() {
    it('should have a table "User"', function (done) {
      db.query('SELECT column_name FROM information_schema.columns WHERE table_name = \'User\'', function (err, res) {
        assert.equal(err, null)
        assert.equal(res.length, 6)
        assert.equal(res[0].column_name, 'id')
        assert.equal(res[1].column_name, 'username')
        assert.equal(res[2].column_name, 'email')
        assert.equal(res[3].column_name, 'password')
        assert.equal(res[4].column_name, 'date_subscribed')
        assert.equal(res[5].column_name, 'date_last_connect')
        done()
      })
    })
    it('should have a table "User_info"', function (done) {
      db.query('SELECT column_name FROM information_schema.columns WHERE table_name = \'User_info\'', function (err, res) {
        assert.equal(err, null)
        assert.equal(res.length, 7)
        assert.equal(res[0].column_name, 'id')
        assert.equal(res[1].column_name, 'user_id')
        assert.equal(res[2].column_name, 'firstname')
        assert.equal(res[3].column_name, 'lastname')
        assert.equal(res[4].column_name, 'nickname')
        assert.equal(res[5].column_name, 'birthdate')
        assert.equal(res[6].column_name, 'gender')
        done()
      })
    })
  })

  describe('API', function () {
    describe('POST /user', function () {
      it('should create a new user', function (done) {
        request.post('/user')
        .send({
          username : 'name_user_add',
          password : '123456',
          birthdate : '31-12-1999',
          email : 'add@test.com'
        })
        .expect(function (res) {
          var data = res.body.data
          assert.deepEqual(Object.keys(data), [
            'id',
            'username',
            'firstname',
            'lastname',
            'nickname',
            'birthdate',
            'email',
            'date_last_connect',
            'date_subscribed'
          ])
          assert.equal(data.id, 1)
          assert.equal(data.username, 'name_user_add')
          db.query('DELETE FROM "User" WHERE id = 1')
          return true
        })
        .end(done)
      })
    })
    describe('DELETE /user', function () {
      it('should send authentication error', function (done) {
        request.delete('/user')
        .expect(400)
        .end(done)
      })
      it('should delete a user', function (done) {
        // Create new user
        request.post('/user')
        .send({
          username : 'name_user_add',
          password : '123456',
          birthdate : '31-12-1999',
          email : 'add@test.com'
        })
        .end(function (err, res) {
          // Get token of the user
          request.post('/oauth/token')
          .set({
            'Authorization' : 'Basic ' + initTest.clientToken,
            'Content-Type' : 'application/x-www-form-urlencoded'
          })
          .send('grant_type=password&username=name_user_add&password=123456')
          .expect(200)
          .end(function (err, res) {
            // Delete the user
            request.delete('/user')
            .set({'Authorization' : 'Bearer ' + res.body.access_token})
            .end(function (err, res) {
              var data = res.body.data
              assert.deepEqual(data, [])
              db.query('SELECT id FROM "User" WHERE id = 1', function (err, rows) {
                assert.equal(rows.length, 0)
              })
              done()
              return true
            })
          })
        })
      })
    })
    describe('GET /users', function () {
      it('should send authentication error', function (done) {
        request.get('/users')
        .expect(400)
        .end(done)
      })
      it('should get list of all users with only their nickname and their last_date_connect', function (done) {
        request.get('/users')
        .set({'Authorization' : 'Bearer ' + initTest.getToken('test_admin')})
        .expect(function (res) {
          var data = res.body.data
          assert.equal(data.length, 10)
          assert.deepEqual(Object.keys(data[0]), [
            'username',
            'date_last_connect',
            'date_subscribed'
          ])
          assert.equal(data[0].username, 'test_admin')
          assert.equal(data[1].username, 'name_user_10')
          assert.equal(data[2].username, 'name_user_20')
          return true
        })
        .end(done)
      })
    })
    describe('GET /user/:user_id', function () {
      it('should send authentication error', function (done) {
        request.get('/user/1')
        .expect(400)
        .end(done)
      })
      it('should get my personnal info', function (done) {
        request.get('/user/10')
        .set({'Authorization' : 'Bearer ' + initTest.getToken('test_admin')})
        .expect(function (res) {
          var data = res.body.data
          assert.deepEqual(Object.keys(data), [
            'id',
            'username',
            'email',
            'date_last_connect',
            'date_subscribed',
            'firstname',
            'lastname',
            'nickname',
            'birthdate',
            'gender'
          ])
          assert.equal(data.id, 10)
          assert.equal(data.username, 'test_admin')
          return true
        })
        .end(done)
      })
      it('should get info of a roomate', function (done) {
        request.get('/user/200')
        .set({'Authorization' : 'Bearer ' + initTest.getToken('name_user_10')})
        .expect(function (res) {
          var data = res.body.data
          assert.deepEqual(Object.keys(data), [
            'id',
            'username',
            'email',
            'date_last_connect',
            'date_subscribed',
            'firstname',
            'lastname',
            'nickname',
            'birthdate',
            'gender'
          ])
          assert.equal(data.id, 200)
          assert.equal(data.username, 'name_user_20')
          return true
        })
        .end(done)
      })
      it('should get partial info of a non roomate', function (done) {
        request.get('/user/500')
        .set({'Authorization' : 'Bearer ' + initTest.getToken('name_user_10')})
        .expect(function (res) {
          var data = res.body.data
          assert.deepEqual(Object.keys(data), [
            'id',
            'username',
            'email',
            'date_last_connect',
            'date_subscribed',
            'firstname',
            'lastname',
            'nickname',
            'birthdate',
            'gender'
          ])
          assert.equal(data.id, 500)
          assert.equal(data.username, 'name_user_50')
          assert.equal(data.firstname, null)
          return true
        })
        .end(done)
      })
      it('should get partial info of a user whithout room', function (done) {
        request.get('/user/900')
        .set({'Authorization' : 'Bearer ' + initTest.getToken('name_user_10')})
        .expect(function (res) {
          var data = res.body.data
          assert.deepEqual(Object.keys(data), [
            'id',
            'username',
            'email',
            'date_last_connect',
            'date_subscribed',
            'firstname',
            'lastname',
            'nickname',
            'birthdate',
            'gender'
          ])
          assert.equal(data.id, 900)
          assert.equal(data.username, 'name_user_90')
          assert.equal(data.firstname, null)
          return true
        })
        .end(done)
      })
    })
    describe('PUT /user', function () {
      it('should send authentication error', function (done) {
        request.put('/user')
        .expect(400)
        .end(done)
      })
      it('should update a user', function (done) {
        request.put('/user')
        .set({'Authorization' : 'Bearer ' + initTest.getToken('name_user_10')})
        .send({
          username  : 'name_user_10_modified',
          password  : '123456',
          email     : 'test',
        })
        .expect(function (res) {
          var data = res.body.data
          assert.equal(data.username, 'name_user_10_modified')
          assert.equal(data.email, 'test')
          db.query('SELECT * FROM "User" WHERE "User".id = 100', function (err, row) {
            assert.equal(row.username, 'name_user_10_modified')
            assert.equal(row.email, 'test')
            assert.equal(row.password, '123456')
            return true
          })
        })
        .end(done)
      })
    })
    describe('PUT /user/info', function () {
      it('should send authentication error', function (done) {
        request.put('/user/info')
        .expect(400)
        .end(done)
      })
      it('should update a user info', function (done) {
        request.put('/user/info')
        .set({'Authorization' : 'Bearer ' + initTest.getToken('name_user_10')})
        .send({
          firstname : 'fname_modified',
          lastname  : 'lname_modified',
          nickname  : 'nname_modified',
          birthdate : (new Date(1980, 10, 20)).toLocaleString(),
          gender    : 'u',
        })
        .expect(function (res) {
          var data = res.body.data
          assert.equal(data.firstname, 'fname_modified')
          assert.equal(data.lastname, 'lname_modified')
          assert.equal(data.nickname, 'nname_modified')
          assert.equal(new Date(data.birthdate).toLocaleString(), (new Date(1980, 10, 20)).toLocaleString())
          assert.equal(data.gender, 'u')
          db.query('SELECT * FROM "User_info" WHERE "User_info".user_id = 100', function (err, row) {
            assert.equal(row.firstname, 'fname_modified')
            assert.equal(row.lastname, 'lname_modified')
            assert.equal(row.nickname, 'nname_modified')
            assert.equal(row.birthdate, (new Date(1980, 10, 20)).toLocaleString())
            assert.equal(row.gender, 'u')
            return true
          })
        })
        .end(done)
      })
    })
  })
})
