'use strict'

var app     = require('../index.js')
var request = require('supertest')(app)
var db      = require('../misc/database')
var config  = require('../config.json')['test']

var clientId = '1E5092C1F80F7F2A1BD897088C17CA53C4F441B9'
var clientSecret = 'F61AA242916F75E3800CDC89B08C51218CAD2FC7'
var clientToken = (new Buffer(clientId + ':' + clientSecret)).toString('base64')
var userToken = {}

var users = [
  'test_admin',
  'name_user_10',
  'name_user_20',
  'name_user_30',
  'name_user_40',
  'name_user_50',
  'name_user_60',
  'name_user_70',
  'name_user_80',
  'name_user_90'
];

before(function (done) {
  this.timeout(10000)
  db.initDB(function () {
    for (var u in users)
      createToken(users[u])
    setTimeout(done, 500)
  })
})

function createToken (user_name) {
  request.post('/oauth/token')
  .set({
    'Authorization' : 'Basic ' + clientToken,
    'Content-Type' : 'application/x-www-form-urlencoded'
  })
  .send('grant_type=password&username='+user_name+'&password=7c4a8d09ca3762af61e59520943dc26494f8941b')
  .expect(200)
  .end(function (err, res) {
    userToken[user_name] = res.body.access_token
  })
}

module.exports = {
  clientToken : clientToken,
  getToken    : function (user_name) { return userToken[user_name] },
  request     : request,
  db          : db
}
