'use strict'

var assert   = require('assert')
var initTest = require('./test_init.js')
var request  = initTest.request
var db       = initTest.db

describe('Roomates --', function () {
  describe('API', function() {
    describe('GET /roommates', function() {
      it('should send an authentication error', function(done) {
        request.get('/roommates')
        .expect(400)
        .end(done)
      })
      it('should get the list of all roommates', function(done) {
        request.get('/roommates')
        .set({'Authorization' : 'Bearer ' + initTest.getToken('test_admin')})
        .expect(function (res) {
          var data = res.body.data
          assert.equal(data.length, 4)
          assert.deepEqual(Object.keys(data[0]), [
            'id',
            'name'
          ])
          assert.equal(data[0].name, 'room_admin' )
          assert.equal(data[1].name, 'room_10'    )
          assert.equal(data[2].name, 'room_20'    )
          assert.equal(data[3].name, ''           )
          return true
        })
        .end(done)
      })
    })
    describe('GET /roommate/:rommate_id', function() {
      it('should send an authentication error', function(done) {
        request.get('/roommate/100')
        .expect(400)
        .end(done)
      })
      it('should get no data', function(done) {
        request.get('/roommate/101')
        .set({'Authorization' : 'Bearer ' + initTest.getToken('name_user_10')})
        .expect(function (res) {
          var data = res.body.data
          assert.deepEqual(data, {})
          return true
        })
        .end(done)
      })
      it('should get info of the roommate whithout user', function(done) {
        request.get('/roommate/100')
        .set({'Authorization' : 'Bearer ' + initTest.getToken('test_admin')})
        .expect(function (res) {
          var data = res.body.data
          assert.equal(data.id, 100)
          assert.equal(data.name, 'room_10')
          assert.equal(data.users, undefined)
          return true
        })
        .end(done)
      })
      it('should get info of the roommate whith user', function(done) {
        request.get('/roommate/100')
        .set({'Authorization' : 'Bearer ' + initTest.getToken('name_user_10')})
        .expect(function (res) {
          var data = res.body.data
          assert.equal(data.id, 100)
          assert.equal(data.name, 'room_10')
          assert.equal(data.users.length, 4)
          assert.equal(data.users[0].id, 100)
          assert.equal(data.users[0].username, 'name_user_10')
          assert.equal(data.users[1].username, 'name_user_20')
          assert.equal(data.users[2].username, 'name_user_30')
          assert.equal(data.users[3].username, 'name_user_40')
          return true
        })
        .end(done)
      })
    })
    describe('PUT /roommate', function() {
      it('should send an authentication error', function(done) {
        request.put('/roommate')
        .expect(400)
        .end(done)
      })
      it('should update roommate infos', function(done) {
        request.put('/roommate')
        .set({'Authorization' : 'Bearer ' + initTest.getToken('name_user_10')})
        .send({
          name : 'room_10_modified'
        })
        .expect(function (res) {
          var data = res.body.data
          assert.equal(data.id, 100)
          assert.equal(data.name, 'room_10_modified')
          assert.equal(data.users.length, 4)
          assert.equal(data.users[0].id, 100)
          assert.equal(data.users[0].username, 'name_user_10')
          assert.equal(data.users[1].username, 'name_user_20')
          assert.equal(data.users[2].username, 'name_user_30')
          assert.equal(data.users[3].username, 'name_user_40')
          return true
        })
        .end(done)
      })
    })
    describe('POST /roommate/leave', function() {
      it('should send an authentication error', function(done) {
        request.post('/roommate/leave')
        .expect(400)
        .end(done)
      })
      it('should leave a roommate', function(done) {
        request.post('/roommate/leave')
        .set({'Authorization' : 'Bearer ' + initTest.getToken('name_user_10')})
        .expect(function (res) {
          assert.deepEqual(res.body.data, {})
          assert.equal(res.body.success, true)
          return true
        })
        .end(done)
      })
      it('should return an error when user is not in roommate', function(done) {
        request.post('/roommate/leave')
        .set({'Authorization' : 'Bearer ' + initTest.getToken('name_user_90')})
        .expect(function (res) {
          assert.deepEqual(res.body.data, {})
          assert.equal(res.body.success, false)
          return true
        })
        .end(done)
      })
    })
    describe('POST /roommate/join', function() {
      it('should send an authentication error', function(done) {
        request.post('/roommate/join')
        .expect(400)
        .end(done)
      })
      it('should join a roommate', function(done) {
        request.post('/roommate/join')
        .set({'Authorization' : 'Bearer ' + initTest.getToken('name_user_10')})
        .send({
          roommate_id : 100
        })
        .expect(function (res) {
          assert.deepEqual(res.body.data, {})
          assert.equal(res.body.success, true)
          return true
        })
        .end(done)
      })
      it('should return an error when user is already in roommate', function(done) {
        request.post('/roommate/join')
        .set({'Authorization' : 'Bearer ' + initTest.getToken('name_user_10')})
        .send({
          roommate_id : 10
        })
        .expect(function (res) {
          assert.deepEqual(res.body.data, {})
          assert.equal(res.body.success, false)
          return true
        })
        .end(done)
      })
    })
    describe('POST /roommate', function() {
      it('should send an authentication error', function(done) {
        request.post('/roommate')
        .expect(400)
        .end(done)
      })
      it('should create a roommate', function(done) {
        request.post('/roommate')
        .set({'Authorization' : 'Bearer ' + initTest.getToken('name_user_90')})
        .send({
          name : 'room_90'
        })
        .expect(function (res) {
          assert.deepEqual(res.body.data, {})
          assert.equal(res.body.success, true)
          return true
        })
        .end(done)
      })
      it('should return an error when user is already in roommate', function(done) {
        request.post('/roommate')
        .set({'Authorization' : 'Bearer ' + initTest.getToken('name_user_10')})
        .send({
          name : 'room_90_none'
        })
        .expect(function (res) {
          assert.deepEqual(res.body.data, {})
          assert.equal(res.body.success, false)
          return true
        })
        .end(done)
      })
    })
  })
})
