'use strict'

var _db = null
var sql = {}

var Oauth =  {
  getAccessToken : function (accessToken, callback) {
    _db.query(sql.getAccessToken, { accessToken : accessToken }, function (err, rows) {
      if (err || rows.length !== 1)
        return callback(err)
      callback(null, {
        accessToken : rows[0].access_token,
        clientId : rows[0].client_id,
        expires : rows[0].date_expires,
        userId : rows[0].user_id
      })
    })
  },
  getClient : function (clientKey, clientSecret, callback) {
    _db.query(sql.getClient, { clientKey : clientKey }, function (err, rows) {
      if (err || rows.length !== 1)
        return callback(err)
      if (clientSecret != null && rows[0].secret != clientSecret)
        return callback()
      callback(null, rows[0])
    })
  },
  getRefreshToken : function (bearerToken, callback) {
    _db.query(sql.getRefreshToken, { refreshToken : bearerToken }, function (err, rows) {
      callback(err, rows.length === 1 ? rows[0] : false)
    })
  },
  grantTypeAllowed : function (clientId, grantType, callback) {
    callback(false, true)
  },
  saveAccessToken : function (accessToken, clientId, expires, user, callback) {
    _db.query(sql.saveAccessToken, {
      accessToken : accessToken,
      clientId : clientId,
      expires : expires,
      userId : user.id
    }, function (err, rows) {
      callback(err)
    })
  },
  saveRefreshToken : function (refreshToken, clientId, expires, user, callback) {
    _db.query(sql.saveRefreshToken, {
      refreshToken : refreshToken,
      clientId : clientId,
      expires : expires,
      userId : user.id
    }, function (err, rows) {
      callback(err)
    })
  },
  getUser : function (username, password, callback) {
    _db.query(sql.getUser, { username : username, password : password }, function (err, rows) {
      callback(err, rows.length === 1 ? rows[0] : false)
    })
  }
}

module.exports = function (db) {
  _db = db
  sql = _db.loadSql(__dirname + '/oauth_sql', {
    getAccessToken : 'getAccessToken.sql',
    getClient : 'getClient.sql',
    saveAccessToken : 'saveAccessToken.sql',
    saveRefreshToken : 'saveRefreshToken.sql',
    getUser : 'getUser.sql'
  })
  return Oauth
}
