SELECT
  "User_access_token".access_token,
  "User_access_token".client_id,
  "User_access_token".date_expires,
  "User_access_token".user_id
FROM "User_access_token"
WHERE "User_access_token".access_token  = :accessToken
