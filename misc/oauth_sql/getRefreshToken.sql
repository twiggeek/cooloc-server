SELECT
  "User_refresh_token".refresh_token,
  "User_refresh_token".id_client,
  "User_refresh_token".date_expires,
  "User_refresh_token".id_user
FROM "User_refresh_token"
WHERE "User_refresh_token".refresh_token  = :refreshToken
