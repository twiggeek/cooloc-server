TRUNCATE
  "Address",
  "Purchase",
  "Refunds",
  "Roommate",
  "User",
  "User_info"
RESTART IDENTITY
CASCADE
;

INSERT INTO "User"
("id", "username"    , "email"            , "password"                                ) VALUES
(10  , 'test_admin'  , 'admin@email.com'  , '7c4a8d09ca3762af61e59520943dc26494f8941b'), /* 123456 */
(100 , 'name_user_10', 'user_10@email.com', '7c4a8d09ca3762af61e59520943dc26494f8941b'), /* 123456 */
(200 , 'name_user_20', 'user_20@email.com', '7c4a8d09ca3762af61e59520943dc26494f8941b'), /* 123456 */
(300 , 'name_user_30', 'user_30@email.com', '7c4a8d09ca3762af61e59520943dc26494f8941b'), /* 123456 */
(400 , 'name_user_40', 'user_40@email.com', '7c4a8d09ca3762af61e59520943dc26494f8941b'), /* 123456 */
(500 , 'name_user_50', 'user_50@email.com', '7c4a8d09ca3762af61e59520943dc26494f8941b'), /* 123456 */
(600 , 'name_user_60', 'user_60@email.com', '7c4a8d09ca3762af61e59520943dc26494f8941b'), /* 123456 */
(700 , 'name_user_70', 'user_70@email.com', '7c4a8d09ca3762af61e59520943dc26494f8941b'), /* 123456 */
(800 , 'name_user_80', 'user_80@email.com', '7c4a8d09ca3762af61e59520943dc26494f8941b'), /* 123456 */
(900 , 'name_user_90', 'user_90@email.com', '7c4a8d09ca3762af61e59520943dc26494f8941b')  /* 123456 */
;

INSERT INTO "User_info"
("user_id", "firstname"    , "lastname"    , "nickname"    , "birthdate" , "gender") VALUES
(10       , 'first_admin'  , 'last_admin'  , 'nick_admin'  , '2000-01-01', 'm'     ),
(100      , 'first_user_10', 'last_user_10', 'nick_user_10', '2000-01-01', 'f'     ),
(200      , 'first_user_20', 'last_user_20', 'nick_user_20', '2001-01-01', 'f'     ),
(300      , 'first_user_30', 'last_user_30', 'nick_user_30', '2001-01-01', 'm'     ),
(400      , 'first_user_40', 'last_user_40', 'nick_user_40', '2001-01-01', 'm'     ),
(500      , 'first_user_50', 'last_user_50', 'nick_user_50', '2001-01-01', 'f'     ),
(600      , 'first_user_60', 'last_user_60', 'nick_user_60', '2001-01-01', 'f'     ),
(700      , DEFAULT        , DEFAULT       , DEFAULT       , DEFAULT     , DEFAULT )
;

INSERT INTO "Address"
("id", "street_name", "street_number", "additional_address", "zip_code" , "city"    , "country" ) VALUES
(10  , 'st_name_1'  , 'st_num_1'     , 'add_addr_1'        , 'zip_001'  , 'ville_1' , 'cntry_1 '),
(100 , 'st_name_10' , 'st_num_10'    , 'add_addr_10'       , 'zip_0010' , 'ville_10', 'cntry_10'),
(200 , 'st_name_20' , 'st_num_20'    , 'add_addr_20'       , 'zip_0020' , 'ville_20', 'cntry_20'),
(300 , DEFAULT      , DEFAULT        , DEFAULT             , DEFAULT    , DEFAULT   , DEFAULT   )
;

INSERT INTO "Roommate"
("id" , "name"      ) VALUES
(10   , 'room_admin'),
(100  , 'room_10'   ),
(200  , 'room_20'   ),
(300  , DEFAULT     )
;

INSERT INTO "Address_roommate"
("address_id", "roommate_id") VALUES
(10          , 10           ),
(100         , 100          ),
(300         , 300          )
;

INSERT INTO "User_roommate"
("user_id", "roommate_id") VALUES
(10          , 10           ),
(100         , 100          ),
(200         , 100          ),
(300         , 100          ),
(400         , 100          ),
(500         , 200          ),
(600         , 200          ),
(700         , 300          ),
(800         , 300          )
;

INSERT INTO "Purchase"
("user_id" , "description" , "price" , "date_created") VALUES
(100       , 'purchase_1_1', 10      , '2014-10-20'),
(100       , 'purchase_1_2', 100     , '2014-10-25'),
(100       , 'purchase_1_3', 1000    , '2014-10-30'),
(100       , 'purchase_1_4', 10000   , '2014-11-05'),
(100       , 'purchase_1_5', 100000  , '2014-11-10'),
(200       , 'purchase_2_1', 10      , '2014-11-01'),
(200       , 'purchase_2_2', 20      , '2014-11-02'),
(200       , 'purchase_2_3', 30      , '2014-11-03'),
(200       , 'purchase_2_4', 40      , '2014-11-04'),
(200       , 'purchase_2_5', 50      , '2014-11-05'),
(200       , 'purchase_2_6', 60      , '2014-11-06'),
(200       , 'purchase_2_7', 70      , '2014-12-07'),
(200       , 'purchase_2_8', 80      , '2014-12-08'),
(200       , 'purchase_2_9', 90      , '2014-12-09'),
(300       , 'purchase_3_1', 10      , '2014-11-01'),
(300       , 'purchase_3_2', 20      , '2014-11-04'),
(300       , 'purchase_3_3', 30      , '2014-11-08'),
(400       , 'purchase_4_1', 40      , '2014-11-12'),
(400       , 'purchase_4_2', 50      , '2014-12-05'),
(400       , 'purchase_4_3', 60      , '2014-11-06'),
(500       , 'purchase_5_1', 70      , '2014-11-07'),
(500       , 'purchase_5_2', 80      , '2014-11-08'),
(500       , 'purchase_5_3', 90      , '2014-11-25'),
(500       , 'purchase_5_4', 90      , '2014-11-26'),
(600       , 'purchase_6_1', 40      , '2014-12-09'),
(600       , 'purchase_6_2', 90      , '2014-12-30')
;

INSERT INTO "Refunds"
("user_from_id" , "user_to_id" , "description"     , "value" , "date_created") VALUES
(200            , 100          , 'Refund_2_to_1_5' , 5       , '2014-11-20'  ),
(200            , 100          , 'Refund_2_to_1_7' , 7       , '2014-11-21'  ),
(200            , 100          , 'Refund_2_to_1_9' , 9       , '2014-11-22'  ),
(300            , 200          , 'Refund_3_to_2_8' , 8       , '2014-11-11'  ),
(400            , 100          , 'Refund_4_to_1_6' , 6       , '2014-11-12'  ),
(200            , 400          , 'Refund_2_to_4_4' , 4       , '2014-11-14'  ),
(500            , 600          , 'Refund_5_to_6_5' , 5       , '2014-11-14'  ),
(500            , 600          , 'Refund_5_to_6_4' , 4       , '2014-11-14'  )
;
