'use strict'

var pg         = require('pg');
var postgrator = require('postgrator')
var fs         = require('fs');
var pgClient   = null;

require('pg-spice').patch(pg);

var db = module.exports = {
  init : function (config, done) {
    if (pgClient != null)
      return done(null)
    var conString = 'pg://' + config.username
                  + ':' + config.password
                  + '@localhost:' + config.port
                  + '/' + config.database
    pgClient = new pg.Client(conString)
    pgClient.connect();
    console.log('Connection to database successful')
    postgrator.setConfig(config)
    postgrator.migrate(config.version, function (err, migrations) {
      if (err)
        throw err
      postgrator.endConnection(function () {
        return done(null)
      })
    })
  },
  loadSql : function (dir, files) {
    var sqls = {}
    for (var file in files)
      sqls[file] = fs.readFileSync(dir + '/' + files[file]).toString()
    return sqls;
  },
  query : function (query, args, done) {
    if (typeof(args) === 'function') {
      done = args
      args = null
    }
    if (pgClient === null) return done('Not connected to database')
    pgClient.query(query, args, function (err, res) {
      var data = []
      if (err) {
        console.error(query)
        console.error(err)
      }
      else
        data = res.rows
      if (done)
        return done(err, data)
    })
  },
  queryFromFile : function (file, truncate, done) {
    if (typeof(truncate) === 'function') {
      done = truncate
      truncate = false
    }
    if (pgClient === null) return done('Not connected to database')
    var that = this
    if (truncate)
      fs.readFile(__dirname + '/truncate_tables.sql', function (err, dataRaw) {
        if (err) throw err
        that.query(dataRaw.toString(), function(err) {
          if (err) throw err
          fs.readFile(file, function (err, dataRaw) {
            if (err) throw err
            var data = dataRaw.toString()
            that.query(data, done)
          })
        })
      })
    else
      fs.readFile(file, function (err, dataRaw) {
        if (err) throw err
        var data = dataRaw.toString()
        that.query(data, done)
      })
  },
  initDB : function (done) {
    if (pgClient === null) return done('Not connected to database')
    var that = this
    fs.readFile(__dirname + '/truncate_tables.sql', function (err, dataRaw) {
      if (err) throw err
      that.query(dataRaw.toString(), function(err) {
        if (err) throw err
        done()
      })
    })
  }
}
