WITH inserted_roommate_user AS (
  INSERT INTO "User_roommate"
  ("user_id", "roommate_id")
  SELECT
    :user_id,
    :roommate_id
  WHERE NOT EXISTS (
    SELECT "user_id" FROM "User_roommate" WHERE "user_id" = :user_id
  )
  RETURNING *
)

SELECT
  COUNT (*)
FROM inserted_roommate_user
