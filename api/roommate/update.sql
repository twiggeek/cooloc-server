WITH updated_roommate AS (
  UPDATE "Roommate" SET
    name  = :name
  WHERE "Roommate"."id" = (
    SELECT
      "User_roommate"."roommate_id"
    FROM "User_roommate"
    WHERE "User_roommate"."user_id" = :user_id
  )
  RETURNING *
)

SELECT
  "User"."id",
  "User"."username",
  "User"."email",
  "User_info"."firstname",
  "User_info"."lastname",
  "User_info"."nickname",
  "User_info"."birthdate",
  "User_info"."gender",
  updated_roommate."id" AS "roommate_id",
  updated_roommate."name"
FROM updated_roommate
INNER JOIN "User_roommate" ON "User_roommate"."roommate_id" = updated_roommate."id"
INNER JOIN "User"          ON "User"."id" = "User_roommate"."user_id"
LEFT JOIN "User_info"      ON "User_info"."user_id" = "User"."id"
