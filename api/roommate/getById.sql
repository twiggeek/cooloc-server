WITH select_roommate_user AS (
  SELECT
    "User_roommate"."roommate_id"
  FROM "User_roommate"
  WHERE "User_roommate"."user_id" = :user_id
),
select_users_in_roommate AS (
  SELECT
    "User"."id",
    "User"."username",
    "User"."email",
    "User_info"."firstname",
    "User_info"."lastname",
    "User_info"."nickname",
    "User_info"."birthdate",
    "User_info"."gender",
    "User_roommate"."roommate_id"
  FROM select_roommate_user
  INNER JOIN "User_roommate" ON "User_roommate"."roommate_id" = select_roommate_user."roommate_id"
  INNER JOIN "User" ON "User_roommate"."user_id" = "User"."id"
  LEFT JOIN "User_info" ON "User_roommate"."user_id" = "User_info"."user_id"
)

SELECT
  "Roommate"."id" AS "roommate_id",
  "Roommate"."name",
  select_users_in_roommate."id",
  select_users_in_roommate."username",
  select_users_in_roommate."email",
  select_users_in_roommate."firstname",
  select_users_in_roommate."lastname",
  select_users_in_roommate."nickname",
  select_users_in_roommate."birthdate",
  select_users_in_roommate."gender"
FROM "Roommate"
LEFT JOIN select_users_in_roommate ON "Roommate"."id" = select_users_in_roommate."roommate_id"
WHERE "Roommate"."id" = :roommate_id
