WITH inserted_roommate AS (
  INSERT INTO "Roommate"
  ("name")
  SELECT
    :name
  WHERE NOT EXISTS (
    SELECT "User_roommate"."user_id" FROM "User_roommate" WHERE "User_roommate"."user_id" = :user_id
  )
  RETURNING *
),
inserted_roommate_user AS (
  INSERT INTO "User_roommate"
  ("user_id", "roommate_id")
  SELECT
    :user_id,
    inserted_roommate."id"
  FROM inserted_roommate
  RETURNING *
)

SELECT
  COUNT(*)
FROM inserted_roommate_user
