'use strict'

var _db = null
var sql = {}

var Roommate = {
  getAll : function (req, res) {
    _db.query(sql.getAll, function (err, rows) {
      if (err) throw err
        res.send({ data : rows })
    })
  },
  getById : function (req, res) {
    _db.query(sql.getById, {
      user_id : req.user.id,
      roommate_id : req.params.roommate_id
    }, function (err, rows) {
      if (err) throw err
      res.send({data : createRoommateWithUsersArray(rows)})
    })
  },
  update : function (req, res) {
    _db.query(sql.update, {
      name  : req.body.name,
      user_id   : req.user.id
    }, function (err, rows) {
      if (err) throw err
      res.send({data : createRoommateWithUsersArray(rows)})
    })
  },
  leave : function (req, res) {
    _db.query(sql.leave, {
      user_id   : req.user.id
    }, function (err, rows) {
      if (err) throw err
      res.send({data : {}, success : rows[0].count == 1})
    })
  },
  join : function (req, res) {
    _db.query(sql.join, {
      roommate_id : req.body.roommate_id,
      user_id   : req.user.id
    }, function (err, rows) {
      if (err) throw err
      res.send({data : {}, success : rows[0].count == 1})
    })
  },
  create : function (req, res) {
    _db.query(sql.create, {
      name : req.body.name,
      user_id   : req.user.id
    }, function (err, rows) {
      if (err) throw err
      res.send({data : {}, success : rows[0].count == 1})
    })
  }
}

exports.init = function (app, db) {
  _db = db
  sql = _db.loadSql(__dirname, {
    getAll : 'getAll.sql',
    update : 'update.sql',
    leave : 'leave.sql',
    join : 'join.sql',
    create : 'create.sql',
    getById : 'getById.sql'
  })
  app.get('/roommates', app.oauth.authorise(), Roommate.getAll)
  app.get('/roommate/:roommate_id', app.oauth.authorise(), Roommate.getById)
  app.put('/roommate', app.oauth.authorise(), Roommate.update)
  app.post('/roommate/leave', app.oauth.authorise(), Roommate.leave)
  app.post('/roommate/join', app.oauth.authorise(), Roommate.join)
  app.post('/roommate', app.oauth.authorise(), Roommate.create)
}

function createRoommateWithUsersArray (rows) {
  if (!rows.length)
    return {}
  var data = {
    id    : rows[0].roommate_id,
    name  : rows[0].name
  }
  if (rows[0].id != null) {
    data.users = []
    for (var i in rows)
      data.users.push({
        id        : rows[i].id,
        username  : rows[i].username,
        email     : rows[i].email,
        firstname : rows[i].firstname,
        lastname  : rows[i].lastname,
        nickname  : rows[i].nickname,
        birthdate : rows[i].birthdate,
        gender    : rows[i].gender
      })
  }
  return data
}
