WITH deleted_roommate AS (
  DELETE FROM "User_roommate"
  WHERE "User_roommate"."user_id" = :user_id
  RETURNING *
)

SELECT COUNT(*) FROM deleted_roommate
