SELECT
  "Purchase"."id",
  "Purchase"."description",
  "Purchase"."price",
  TO_CHAR("Purchase"."date_created", 'DD-MM-YYYY') AS "date_created",
  "User_info"."user_id",
  "User_info"."firstname",
  "User_info"."lastname",
  "User_info"."nickname",
  TO_CHAR("User_info"."birthdate", 'DD-MM-YYYY') AS "birthdate"
FROM "Purchase"
INNER JOIN "User_info" ON "Purchase"."user_id" = "User_info"."user_id"
ORDER BY "Purchase"."date_created" DESC
