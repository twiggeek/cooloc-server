'use strict'

var _db = null
var sql = {}

var Purchase = {
  getAll : function (req, res) {
    _db.query(sql.getAll, function (err, rows) {
      if (err) throw err
      res.send({ data : rows })
    })
  }
}

exports.init = function (app, db) {
  _db = db
  sql = _db.loadSql(__dirname, {
    getAll : 'getAll.sql'
  })
  app.get('/purchases', app.oauth.authorise(), Purchase.getAll)
}
