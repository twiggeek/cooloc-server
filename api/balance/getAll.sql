WITH sum_values_by_user AS (
  SELECT
    COALESCE(SUM("Purchase"."price"), 0)                              AS "sum_price",
    COALESCE(SUM("Refunds"."value"), 0)                               AS "sum_refunds",
    TO_CHAR(MAX("Purchase"."date_created"), 'MM-DD-YYYY HH24:MI:SS')  AS "last_purchase",
    TO_CHAR(MAX("Refunds"."date_created"), 'MM-DD-YYYY HH24:MI:SS')   AS "last_refund",
    "User_info"."user_id",
    "User_info"."firstname",
    "User_info"."lastname",
    "User_info"."nickname"
  FROM "Purchase"
  LEFT  JOIN "Refunds"    ON "Purchase"."user_id" = "Refunds"."user_from_id"
  INNER JOIN "User_info"  ON "Purchase"."user_id" = "User_info"."user_id"
  GROUP BY "User_info"."user_id", "firstname", "lastname", "nickname"
)

SELECT
  *,
  sum_price - sum_refunds AS "balance",
  GREATEST(last_purchase, last_refund) AS "last_action"
FROM sum_values_by_user
