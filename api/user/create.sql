WITH inserted_user AS (
  INSERT INTO "User"
  ("username", "password", "email") VALUES
  (:username , :password , :email )
  RETURNING "User".*
)

SELECT
  inserted_user.id,
  inserted_user.username,
  "User_info".firstname,
  "User_info".lastname,
  "User_info".nickname,
  "User_info".birthdate,
  inserted_user.email,
  inserted_user.date_last_connect,
  inserted_user.date_subscribed
FROM inserted_user
LEFT JOIN "User_info" ON inserted_user.id = "User_info".user_id

