SELECT
  "User"."username",
  TO_CHAR("User"."date_last_connect", 'DD-MM-YYYY') AS "date_last_connect",
  TO_CHAR("User"."date_subscribed", 'DD-MM-YYYY') AS "date_subscribed"
FROM "User"
ORDER BY "User"."date_last_connect"
