WITH param_user AS (
  SELECT (CAST (:user_id AS NUMERIC)) AS id
),
deleted_access_token AS (
  DELETE FROM "User_access_token"
  USING param_user
  WHERE "User_access_token".user_id = param_user.id
  RETURNING param_user.id
),
deleted_refresh_token AS (
  DELETE FROM "User_refresh_token"
  USING param_user
  WHERE "User_refresh_token".user_id = param_user.id
  RETURNING param_user.id
),
deleted_user_info AS (
  DELETE FROM "User_info"
  USING param_user
  WHERE "User_info".user_id = param_user.id
  RETURNING param_user.id
),
deleted_user AS (
  DELETE FROM "User"
  USING param_user
  WHERE "User".id = param_user.id
  RETURNING param_user.id
)

SELECT *
FROM param_user, deleted_access_token, deleted_refresh_token, deleted_user_info
