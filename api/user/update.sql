UPDATE "User" SET
  username  = :username,
  email     = :email,
  password  = :password
WHERE "User"."id" = :user_id
RETURNING *
