'use strict'

var _db = null
var sql = {}

var User = {
  create : function (req, res) {
    _db.query(sql.create, {
      username  : req.body.username,
      password  : req.body.password,
      email     : req.body.email
    }, function (err, rows) {
      if (err) throw err
      res.send({ data : rows[0] })
    })
  },
  update : function (req, res) {
    _db.query(sql.update, {
      username  : req.body.username,
      password  : req.body.password,
      email     : req.body.email,
      user_id   : req.user.id
    }, function (err, rows) {
      if (err) throw err
      res.send({ data : rows[0] })
    })
  },
  updateInfo : function (req, res) {
    _db.query(sql.updateInfo, {
      firstname : req.body.firstname,
      lastname  : req.body.lastname,
      nickname  : req.body.nickname,
      birthdate : req.body.birthdate,
      gender    : req.body.gender,
      user_id   : req.user.id
    }, function (err, rows) {
      if (err) throw err
      res.send({ data : rows[0] })
    })
  },
  delete : function (req, res) {
    _db.query(sql.delete, { user_id  : req.user.id }, function (err, rows) {
      if (err) throw err
      res.send({ data : rows })
    })
  },
  getById : function (req, res) {
    _db.query(sql.getById, {
      user_id        : req.user.id,
      user_asked_id  : req.params.user_id
    }, function (err, rows) {
      if (err) throw err
      res.send({ data : rows[0] })
    })
  },
  getAll : function (req, res) {
    _db.query(sql.getAll, function (err, rows) {
      if (err) throw err
      res.send({ data : rows })
    })
  }
}

exports.init = function (app, db) {
  _db = db
  sql = _db.loadSql(__dirname, {
    create      : 'create.sql',
    delete      : 'delete.sql',
    update      : 'update.sql',
    updateInfo  : 'updateInfo.sql',
    getById     : 'getById.sql',
    getAll      : 'getAll.sql'
  })
  app.post('/user', User.create)
  app.delete('/user', app.oauth.authorise(), User.delete)
  app.put('/user', app.oauth.authorise(), User.update)
  app.put('/user/info', app.oauth.authorise(), User.updateInfo)
  app.get('/user/:user_id', app.oauth.authorise(), User.getById)
  app.get('/users', app.oauth.authorise(), User.getAll)
}
