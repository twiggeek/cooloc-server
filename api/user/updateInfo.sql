UPDATE "User_info" SET
  firstname = :firstname::TEXT,
  lastname  = :lastname::TEXT,
  nickname  = :nickname::TEXT,
  birthdate = :birthdate::DATE,
  gender    = :gender::CHAR
WHERE "User_info"."user_id" = :user_id
RETURNING *
