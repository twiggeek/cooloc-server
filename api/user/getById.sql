WITH selected_user AS (
  SELECT
    "User"."id",
    "User"."username",
    "User"."email",
    "User"."date_last_connect",
    "User"."date_subscribed",
    "User_roommate"."roommate_id" AS "roommate_id_asked"
  FROM "User"
  LEFT JOIN "User_roommate" ON "User_roommate"."user_id" = :user_asked_id
  WHERE "User"."id" = :user_asked_id
),
users_in_roomate AS (
  SELECT
    "User_info"."firstname",
    "User_info"."lastname",
    "User_info"."nickname",
    "User_info"."birthdate",
    "User_info"."gender",
    "User_roommate"."roommate_id" AS "roommate_id_cur"
  FROM "User_info"
  LEFT JOIN "User_roommate" ON "User_roommate"."user_id" = :user_id
)

SELECT
  selected_user."id",
  selected_user."username",
  selected_user."email",
  selected_user."date_last_connect",
  selected_user."date_subscribed",
  users_in_roomate."firstname",
  users_in_roomate."lastname",
  users_in_roomate."nickname",
  TO_CHAR(users_in_roomate."birthdate", 'DD-MM-YYYY') AS "birthdate",
  users_in_roomate."gender"
FROM selected_user
LEFT JOIN users_in_roomate ON users_in_roomate."roommate_id_cur" = selected_user."roommate_id_asked"
