'use strict'

var fs          = require('fs')
var path        = require('path')
var express     = require('express')
var configFile  = require('./config.json')
var db          = require('./misc/database')
var oauthserver = require('oauth2-server')
var bodyParser  = require('body-parser')

var app = express()
app.use(bodyParser.urlencoded({extended : true}))
app.use(bodyParser.json())

var options = require('minimist')(process.argv.slice(2))
var mode    =  options.dev  ? 'dev'
             : options.prod ? 'prod'
                            : 'test'

var config  = configFile[mode]

app.get('/', function (req, res) {
  res.send('Hello world!')
})

db.init(config.database, function (err) {
  if (err) throw err

  app.oauth = oauthserver({
    model : require('./misc/oauth')(db),
    grants : ['password'],
    debug : mode == 'prod'
  })

  app.all('/oauth/token', app.oauth.grant())


  fs.readdir(path.join(__dirname, '/api'), function (err, files) {
    if (err) throw err
    for (var i = 0; i < files.length; i++) {
      require('./api/' + files[i] + '/index.js').init(app, db)
      console.log('API ' + files[i] + ' loaded !')
    }
    app.use(app.oauth.errorHandler())
  })
})

module.exports = app;

app.listen(config.server.port);
