CREATE TABLE "Refunds" (
  "id"            SERIAL                      NOT NULL,
  "user_from_id"  INT2                        REFERENCES "User" ("id"),
  "user_to_id"    INT2                        REFERENCES "User" ("id"),
  "description"   VARCHAR(255)                NOT NULL DEFAULT '',
  "value"         DECIMAL                     NOT NULL,
  "date_created"  TIMESTAMP                   NOT NULL DEFAULT current_timestamp,
  CONSTRAINT "Refunds_value_positive"         CHECK ("value" > 0),
  CONSTRAINT "Refunds_PK"                     PRIMARY KEY ("id")
);
