CREATE TABLE "User" (
  "id"                  SERIAL          NOT NULL,
  "username"            VARCHAR(50)     NOT NULL,
  "email"               VARCHAR(50)     NOT NULL,
  "password"            VARCHAR(50)     NOT NULL,
  "date_subscribed"     TIMESTAMP       NOT NULL DEFAULT current_timestamp,
  "date_last_connect"   TIMESTAMP       NOT NULL DEFAULT current_timestamp,
  CONSTRAINT "User_Mail_UK"             UNIQUE      ("email"),
  CONSTRAINT "User_Username_UK"         UNIQUE      ("username"),
  CONSTRAINT "User_PK"                  PRIMARY KEY ("id")
);

CREATE INDEX users_username_password ON "User" USING btree ("username", "password");

CREATE TABLE "User_info" (
  "id"                  SERIAL          NOT NULL,
  "user_id"             INT2            NOT NULL REFERENCES "User"      ("id"),
  "firstname"           VARCHAR(50)     NOT NULL DEFAULT '',
  "lastname"            VARCHAR(50)     NOT NULL DEFAULT '',
  "nickname"            VARCHAR(50)     NOT NULL DEFAULT '',
  "birthdate"           DATE            NOT NULL DEFAULT '1789-07-14',
  "gender"              VARCHAR(1)      NOT NULL DEFAULT 'u',
  CONSTRAINT "User_info_UK"             UNIQUE      ("user_id"),
  CONSTRAINT "User_info_PK"             PRIMARY KEY ("id")
);

CREATE TABLE "Roommate" (
  "id"                  SERIAL          NOT NULL,
  "name"                VARCHAR(255)    NOT NULL DEFAULT '',
  CONSTRAINT "Roommate_PK"              PRIMARY KEY ("id")
);

CREATE TABLE "Address" (
  "id"                  SERIAL          NOT NULL,
  "street_name"         VARCHAR(255)    NOT NULL DEFAULT '',
  "street_number"       VARCHAR(20)     NOT NULL DEFAULT '',
  "additional_address"  VARCHAR(255)    NOT NULL DEFAULT '',
  "zip_code"            VARCHAR(10)     NOT NULL DEFAULT '',
  "city"                VARCHAR(255)    NOT NULL DEFAULT '',
  "country"             VARCHAR(100)    NOT NULL DEFAULT '',
  CONSTRAINT "Address_PK"               PRIMARY KEY ("id")
);

CREATE TABLE "Address_roommate" (
  "address_id"          INT2            NOT NULL REFERENCES "Address"   ("id"),
  "roommate_id"         INT2            NOT NULL REFERENCES "Roommate"  ("id")
);

CREATE TABLE "User_roommate" (
  "user_id"             INT2            NOT NULL REFERENCES "User"      ("id"),
  "roommate_id"         INT2            NOT NULL REFERENCES "Roommate"  ("id")
);

CREATE TABLE "Purchase" (
  "id"                  SERIAL          NOT NULL,
  "user_id"             INT2            NOT NULL REFERENCES "User" ("id"),
  "description"         VARCHAR(255)    NOT NULL DEFAULT '',
  "price"               DECIMAL         NOT NULL,
  "date_created"        TIMESTAMP       NOT NULL DEFAULT current_timestamp,
  CONSTRAINT "Purchase_price_positive"  CHECK       ("price" > 0),
  CONSTRAINT "Purchase_PK"              PRIMARY KEY ("id")
);
