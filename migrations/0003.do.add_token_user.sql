CREATE TABLE "Client_token" (
  "name"          VARCHAR(255)  NOT NULL,
  "key"           VARCHAR(255)  NOT NULL,
  "secret"        VARCHAR(255)  NOT NULL,
  "redirect_uri"  TEXT          NOT NULL,
  "date_created"  TIMESTAMP     NOT NULL DEFAULT current_timestamp,
  CONSTRAINT "Client_token_PK"  PRIMARY KEY ("key")
);

/**
 * Key generated with `chance.hash({casing : 'upper'})` at chancejs.com
 */
INSERT INTO "Client_token"
("name"   , "key"                                     , "secret"                                  , "redirect_uri") VALUES
('desktop', '1E5092C1F80F7F2A1BD897088C17CA53C4F441B9', 'F61AA242916F75E3800CDC89B08C51218CAD2FC7', '/'           )
;

CREATE TABLE "User_access_token" (
  "user_id"       INT2              REFERENCES "User"         ("id"),
  "client_id"     VARCHAR(255)      REFERENCES "Client_token" ("key"),
  "access_token"  VARCHAR(255)      NOT NULL,
  "date_expires"  TIMESTAMP         NOT NULL DEFAULT current_timestamp,
  "date_created"  TIMESTAMP         NOT NULL DEFAULT current_timestamp,
  CONSTRAINT "User_access_token_PK" PRIMARY KEY ("access_token")
);

CREATE TABLE "User_refresh_token" (
  "user_id"       INT2                REFERENCES "User"         ("id"),
  "client_id"     VARCHAR(255)        REFERENCES "Client_token" ("key"),
  "refresh_token" VARCHAR(255)        NOT NULL,
  "date_expires"  TIMESTAMP           NOT NULL DEFAULT current_timestamp,
  "date_created"  TIMESTAMP           NOT NULL DEFAULT current_timestamp,
  CONSTRAINT "User_refresh_token_PK"  PRIMARY KEY ("refresh_token")
);
